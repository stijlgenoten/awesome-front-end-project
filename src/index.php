<!DOCTYPE html>
<html lang="nl">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Awesom Front-end Project</title>
	<script src="js/main.js"></script>
	<link rel="stylesheet" href="css/main.css"></link>

	<script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
</head>
<body>
	<header class="header">
		<!-- grey top -->
		<div class="header__top">
			<a href="#" class="header__top-button">
				<svg class="header__top-button-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 27.147 27.147"><g data-name="Component 6 – 1"><g data-name="Group 8"><g data-name="Group 7"><path data-name="Path 7" d="M13.573 27.147a13.573 13.573 0 1113.573-13.573 13.589 13.589 0 01-13.573 13.573zm0-25.881A12.307 12.307 0 1025.88 13.573 12.321 12.321 0 0013.573 1.266z" fill="#428cc4"/></g></g><g data-name="Group 9"><path data-name="Path 8" d="M21.685 23.678c-.932-4.935-3.153-8.725-5.942-10.137l-.923-.467.837-.609a3.812 3.812 0 10-4.472 0l.837.608-.923.469c-2.7 1.37-4.889 5.015-5.86 9.749l-1.24-.254c.956-4.66 3.071-8.374 5.741-10.162a5.077 5.077 0 117.363-.006c2.761 1.839 4.908 5.706 5.826 10.572z" fill="#428cc4"/></g></g></svg>
				<span class="header__top-button-label">Mijn profiel</span>
			</a>
		</div>

		<!-- navigation -->
		<div class="grid-container">
			<div class="container || header__nav || flex justify-content-between align-items-center">
				<!-- logo -->
				<div>
					<img class="header__nav-logo" src="/assets/images/logo.svg" />
				</div>

				<!-- menu -->
				<nav>
					<ul class="header__nav-menu">
						<li class="header__nav-menu-item">
							<a class="header__nav-menu-link" href="#">
								Onze vactures
							</a>
						</li>

						<li class="header__nav-menu-item">
							<a class="header__nav-menu-link" href="#">
								Over Deltawonen
							</a>
						</li>
					</ul>
				</nav>

				<!-- search -->
				<div>
					<form class="header__nav-search || flex">
						<input class="header__nav-search-input" type="text" placeholder="Zoek op vacature" />
						<button class="header__nav-search-button" type="submit">
							<svg class="header__nav-search-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.458 21.462"><g data-name="Group 13"><g data-name="Group 11"><path data-name="Path 11" d="M21.272 20.367l-5.216-5.216a9.158 9.158 0 10-.9.9l5.216 5.216a.648.648 0 00.452.19.627.627 0 00.452-.19.644.644 0 000-.9zM1.28 9.15a7.866 7.866 0 117.866 7.871A7.875 7.875 0 011.28 9.15zm0 0" fill="#d3004f"/></g></g></svg>
						</button>
					</form>
				</div>

				<!-- hamburger -->
				<div class="header__nav-hamburger">
					<svg class="header__nav-hamburger-inner" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="none" stroke="#000" stroke-linecap="round" stroke-miterlimit="10" stroke-width="32" d="M80 160h352M80 256h352M80 352h352"/></svg>
				</div>
			</div>
		</div>
	</header>

	<section class="grid-container">
		<div class="container || flex || hero">
			<div class="container__inner || flex align-self-end">
				<div class="hero__text">	
					Tempor invidunt ut labore et dolore magna aliquyam erat, 
					sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. 
					Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.	
				</div>
			</div>
			<div class="container__inner || hero__blue">
				<div class="hero__inner">					
					<h1 class="text-white || hero__title">
						Samen werken <span class="block || text-secundary">aan geluk</span>
					</h1>		
					
					<h2 class="text-white || hero__subtitle">Groei in je nieuwe rol</h2>

					<p class="text-white">
						Tempor invidunt ut labore et dolore magna aliquyam erat, 
						sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. 
						Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. 
					</p>
				</div>
			</div>
		</div>
	</section>

	<section class="grid-container">
		<div class="container || flex">
			<div class="container__inner || background background--cover" style="background-image: url('assets/images/people.png');">				
			</div>
			<div class="container__inner">
				<div class="workfloor">
					<h3 class="text-grey || workfloor__subtitle">Op de werkvloer</h3>
					<h2 class="text-secundary || workfloor__title">Iedere dag is weer anders bij Delta Wonen</h2>
					<p>Tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur.</p>
					<a href="#" class="btn btn--secundary || align-self-end || workfloor__button">Ons verhaal</a>
				</div>
			</div>
		</div>
	</section>	

	<!-- <section class="grid-container">
		<div class="container || hero">
			<div class="container__inner">		
				Tempor invidunt ut labore et dolore magna aliquyam erat, 
				sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. 
				Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. 
				Lorem ipsum dolor sit amet.		
			</div>
			<div class="container__inner container__inner-padding || flex flex-column || hero__blue">
				<h1 class="hero__title">Samen werken <span class="text-secundary">aan geluk</span></h1>
			</div>
		</div>
	</section>

	<section class="grid-container">
		<div class="container || flex">
			<div class="container__inner || background background--cover" style="background-image: url('assets/images/people.png');">				
			</div>
			<div class="container__inner container__inner-padding || flex flex-column || workfloor">
				<h3 class="text-grey || workfloor__subtitle">Op de werkvloer</h3>
				<h2 class="text-secundary || workfloor__title">Iedere dag is weer anders bij Delta Wonen</h2>
				<p>Tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur.</p>
				<a href="#" class="btn btn--secundary || align-self-end || workfloor__button">Ons verhaal</a>
			</div>
		</div>
	</section> -->

	<footer>
	</footer>

</body>
</html>