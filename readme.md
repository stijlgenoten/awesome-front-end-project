## Getting started

You'll need the following installed before continuing:

1. [Node.js](http://nodejs.org): Use the installer provided on the NodeJS website.

## Installation

1. Clone the repository to your local computer.
2. Navigate with your terminal to the repository.
3. Type **npm install** to install all node dependencies.

## Developing

For local development you can run **gulp watch** to start the development server.