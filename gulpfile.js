// plugins
var gulp = require('gulp'),
	sass = require('gulp-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	autoprefixer = require('gulp-autoprefixer'),
	imagemin = require('gulp-imagemin'),
	cache = require('gulp-cache'),
	concat = require('gulp-concat'),
	del = require('del'),
	path = require('path');

// global
const distPath = './dist';

// tasks
gulp.task('clean', function () {
	return del([
		distPath + '/**/*.*',
	]);
});

gulp.task('html', function() {
	return gulp.src('src/**/*.html')
		.pipe(gulp.dest(distPath));
});

gulp.task('php', function() {
	return gulp.src('src/**/*.php')
		.pipe(gulp.dest(distPath));
});

gulp.task('styles', function() {
	return gulp.src('src/scss/*.scss')
		.pipe(sourcemaps.init())
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(sourcemaps.write('./maps'))
		.pipe(gulp.dest(path.join(distPath, '/css')));
});

gulp.task('images', function(){
	return gulp.src('src/images/*.+(png|jpg|gif|svg)')
		.pipe(cache(imagemin({
			progressive: true
		})))
		.pipe(gulp.dest(path.join(distPath + '/images')));
});

gulp.task('scripts', function() {
	return gulp.src(['src/js/main.js'])
		.pipe(sourcemaps.init())
		.pipe(concat('main.js'))
		.pipe(sourcemaps.write('./maps'))
		.pipe(gulp.dest(path.join(distPath + '/js')));
});

gulp.task('watch', function() {
	gulp.watch('src/**/*.php', gulp.series('php'));
	gulp.watch('src/**/*.html', gulp.series('html'));
	gulp.watch('src/scss/*.scss', gulp.series('styles'));
	gulp.watch('src/js/**/*.js', gulp.series('scripts'));
	gulp.watch('src/images/*.+(png|jpg|gif|svg)', gulp.series('images'));
});

gulp.task('default', gulp.series('clean', gulp.parallel('html', 'styles', 'images', 'scripts'), 'watch'));
gulp.task('build', gulp.series('clean', gulp.parallel('styles')));